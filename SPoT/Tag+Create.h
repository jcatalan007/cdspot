//
//  Tag+Create.h
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Tag.h"

@interface Tag (Create)

+ (Tag *)tagWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSString *)catchallTagName;

@property (nonatomic,strong,readonly) NSString *section; // calculated property
@property (nonatomic,strong,readonly) NSString *capitalizedName; // calculated property
@property (nonatomic,strong,readonly) NSSet *displayedPhotos;

@end
