//
//  Photo.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 15/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Photo.h"
#import "Tag.h"


@implementation Photo

@dynamic dateStamp;
@dynamic displayed;
@dynamic imageURL;
@dynamic subtitle;
@dynamic thumbnail;
@dynamic thumbnailURL;
@dynamic title;
@dynamic unique;
@dynamic tags;

@end
