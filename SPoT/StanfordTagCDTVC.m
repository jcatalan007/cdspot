//
//  StanfordTagCDTVC.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "StanfordTagCDTVC.h"
#import "FlickrFetcher.h"
#import "Photo+Flickr.h"
#import "SingletonManagedDocument.h"

@implementation StanfordTagCDTVC

- (IBAction)loadPhotosFromFlicker
{
    [self.refreshControl beginRefreshing];
    dispatch_queue_t loaderQ = dispatch_queue_create("flicker loader", NULL);
    dispatch_async(loaderQ, ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSArray *stanfordPhotos = [FlickrFetcher stanfordPhotos];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        // put the photos in Core Data database
        [self.managedObjectContext performBlock:^{
            for (NSDictionary *photo in stanfordPhotos) {
                [Photo photoWithFlickrInfo:photo inManagedObjectContext:self.managedObjectContext];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.refreshControl endRefreshing];
            });
        }];
    });
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.refreshControl addTarget:self action:@selector(loadPhotosFromFlicker) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!self.managedObjectContext)
        [self useStanfordDocument];
    // perform the fetch again (in case we deleted any photos)
    [self performFetch];
}

- (void)useStanfordDocument
{
    SingletonManagedDocument *dataHelper = [SingletonManagedDocument sharedManagedDocument];
    UIManagedDocument *document = dataHelper.sharedDocument;
    NSURL *url = document.fileURL;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[url path]]) {
        // create it
        [document saveToURL:url forSaveOperation:UIDocumentSaveForCreating completionHandler:^(BOOL success){
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
                [self loadPhotosFromFlicker];
            }
        }];
    } else if (document.documentState == UIDocumentStateClosed) {
        // open it
        [document openWithCompletionHandler:^(BOOL success){
            if (success) {
                self.managedObjectContext = document.managedObjectContext;
            }
        }];
    } else {
        // try to use it
        self.managedObjectContext = document.managedObjectContext;
    }
}

@end
