//
//  Photo+Flickr.h
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Photo.h"

@interface Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary
        inManagedObjectContext:(NSManagedObjectContext *)context;

@property (nonatomic,strong,readonly) NSString *section; // calculated property

@end
