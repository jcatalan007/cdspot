//
//  ViewController.m
//  SPoT
//
//  Created by Juan C. Catalan on 22/02/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageCache.h"

@interface ImageViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *titleBarButtonItem;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UIBarButtonItem *splitViewBarButtonItem; // only used for iPad UI
@property (nonatomic, getter = isAutoZoomed) BOOL autoZoomed;

@end

@implementation ImageViewController

#pragma mark - Toolbar control

// Puts the splitViewBarButton in our toolbar (and/or removes the old one).
// Must be called when our splitViewBarButtonItem property changes
//  (and also after our view has been loaded from the storyboard (viewDidLoad)).
- (void)handleSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    if (_splitViewBarButtonItem)
        [toolbarItems removeObject:_splitViewBarButtonItem]; // if the barbutton is present remove it
    if (barButtonItem)
        [toolbarItems insertObject:barButtonItem atIndex:0]; // put the barbutton on the left of our existing toolbar
    self.toolbar.items = toolbarItems;
    _splitViewBarButtonItem = barButtonItem;
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    if (barButtonItem != _splitViewBarButtonItem) {
        [self handleSplitViewBarButtonItem:barButtonItem];
    }
}

// set title for the toolbar
- (void)setTitle:(NSString *)title
{
    super.title = title;
    self.titleBarButtonItem.title = title;
}

// resets the image whenever the URL changes
- (void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    [self resetImage];
}

# pragma mark - View control

// fetches the data from the URL
// turns it into an image
// adjusts the scroll view's content size to fit the image
// sets the image as the image view's image
- (void)resetImage
{
    if (self.scrollView && self.imageURL) {
        self.scrollView.contentSize = CGSizeZero;
        self.imageView.image = nil;
        self.autoZoomed = YES;
        // fetch the image in another thread
        [self.spinner startAnimating];
        NSURL *imageURL = self.imageURL;
        dispatch_queue_t imageFecthQ = dispatch_queue_create("image fetcher", NULL);
        dispatch_async(imageFecthQ, ^{
            NSData *imageData = [ImageCache imageFromURL:self.imageURL]; // possible network activity!
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            // we check if the current image is the image we wanted before connecting to the network to check for the case where the user just cancelled the image and selected another one
            if (self.imageURL == imageURL) {
                // update the image in the cache
                [ImageCache storeImage:imageData forURL:imageURL];
                // UI has to be changed in the main thread, so we dispatch it in the main queue
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (image) {
                        self.scrollView.contentSize = image.size;
                        self.imageView.image = image;
                        self.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
                        [self zoomScaleToFit];
                    }
                    [self.spinner stopAnimating];
                });
            }
        });
    }
}

// lazy instantiation
- (UIImageView *)imageView
{
    if (!_imageView) _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    return _imageView;
}

// returns the view which will be zoomed when the user pinches
// in this case, it is the image view, obviously
// (there are no other subviews of the scroll view in its content area)
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

// Disable autoZoom after the user performs a zoom (by pinching) 
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    self.autoZoomed = NO;    
}

// Calculate zoom scale to fit the image in the screen without blank spaces
- (void)zoomScaleToFit
{
    // if is AutoZoomed then set the zoomScale property only if the geometry is completely loaded 
    if ((self.isAutoZoomed)&&(self.imageView.bounds.size.width)&&(self.scrollView.bounds.size.width)) {
        CGFloat widthRatio  = self.scrollView.bounds.size.width  / self.imageView.bounds.size.width;
        CGFloat heightRatio = self.scrollView.bounds.size.height / self.imageView.bounds.size.height;
        self.scrollView.zoomScale = (widthRatio > heightRatio) ? widthRatio : heightRatio;
    }
}

// When subviews are layed out we set the zoom to make the image fit the screen.
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self zoomScaleToFit];
}

// add the image view to the scroll view's content area
// setup zooming by setting min and max zoom scale
// and setting self to be the scroll view's delegate
// resets the image in case URL was set before outlets (e.g. scroll view) were set
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.scrollView addSubview:self.imageView];
    self.scrollView.minimumZoomScale = 0.2;
    self.scrollView.maximumZoomScale = 5.0;
    self.scrollView.delegate = self;
    [self resetImage];
    self.titleBarButtonItem.title = self.title;
    [self handleSplitViewBarButtonItem:self.splitViewBarButtonItem]; // necessary to set the button in the toolbar in the iPad UI
}

@end