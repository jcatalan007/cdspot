//
//  PhotosCDTVC.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 12/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "PhotosCDTVC.h"
#import "Photo.h"
#import "PhotoCell.h"

@interface PhotosCDTVC() <UISplitViewControllerDelegate>

@end

@implementation PhotosCDTVC

- (Photo *)photoForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.fetchedResultsController objectAtIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Photo"];
    cell.imageView.image = nil; // because we reuse cells we have to clear the image shown
    Photo *photo = [self photoForRowAtIndexPath:indexPath];
    cell.textLabel.text = photo.title;
    cell.detailTextLabel.text = photo.subtitle;
    // keep the id for the photo to display
    cell.photoId = photo.unique;
    // if the thumbnail is not in the database then get it from Flickr
    if (!photo.thumbnail) {
        dispatch_queue_t thumbnailQ = dispatch_queue_create("thumbnail loader", NULL);
        dispatch_async(thumbnailQ, ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            NSURL *url = [NSURL URLWithString:photo.thumbnailURL];
            NSData *thumbnailData = [[NSData alloc] initWithContentsOfURL:url];
            /*
            // This delay is introduced to test multithreaded code
            [NSThread sleepForTimeInterval:1.5]; 
            */
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            // store the thumbnail in the database
            [photo.managedObjectContext performBlock:^{
                photo.thumbnail = thumbnailData;
            }];
            // put the thumbnail fetched from Flickr in the cell imageView (if needed)
            dispatch_async(dispatch_get_main_queue(), ^{
                // if the cell photoId doesn't match the photo unique identifier
                // this means that the cell is used for another photo
                if ([cell.photoId isEqualToString:photo.unique]) {
                    cell.imageView.image = [[UIImage alloc] initWithData:thumbnailData];
                    [cell setNeedsLayout];
                }
            });
        });
    } else
        cell.imageView.image = [[UIImage alloc] initWithData:photo.thumbnail];
    return cell;
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString:@"setImageURL:"]) {
                if ([segue.destinationViewController respondsToSelector:@selector(setImageURL:)]) {
                    [self transferSplitViewBarButtonItemToViewController:segue.destinationViewController];
                    Photo *photo = [self photoForRowAtIndexPath:indexPath];
                    NSURL *url = [NSURL URLWithString:photo.imageURL];
                    [segue.destinationViewController performSelector:@selector(setImageURL:) withObject:url];
                    [segue.destinationViewController setTitle:photo.title];
                    // set the date stamp of the photo that will provide the order for the recents tab
                    photo.dateStamp = [NSDate date];
                }
            }
        }
    }
}

- (void)removePhotoFromDisplayedPhotosForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Photo *photo = [self photoForRowAtIndexPath:indexPath];
    photo.displayed = @(NO);
    [photo.tags makeObjectsPerformSelector:@selector(adjustTagVisibility)];
}

// To enable the swipe-to-delete feature of table views (wherein a user swipes horizontally across
// a row to display a Delete button), you must implement this method.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete){
        /* 'Smooth' removal of photos not implemented
         * By 'smooth' I mean that the cell disappears with an animation
         * and that the scrollview doesn't change position
         */        
        [self removePhotoFromDisplayedPhotosForRowAtIndexPath:indexPath];
        [self.fetchedResultsController performFetch:nil];
        [self.tableView reloadData];
    }
}

#pragma mark - UISplitViewControllerDelegate

- (void) awakeFromNib
{
    self.splitViewController.delegate = self;
}

// hide detail VC in portrait orientation / show it in landscape orientation
- (BOOL) splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = @"Photos";
    id detailViewController = [self.splitViewController.viewControllers lastObject];
    if ([detailViewController respondsToSelector:@selector(setSplitViewBarButtonItem:)])
        [detailViewController performSelector:@selector(setSplitViewBarButtonItem:) withObject:barButtonItem];
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button
    id detailViewController = [self.splitViewController.viewControllers lastObject];
    if ([detailViewController respondsToSelector:@selector(setSplitViewBarButtonItem:)])
        [detailViewController performSelector:@selector(setSplitViewBarButtonItem:) withObject:nil];
}

# pragma mark - Split view: transfer button

- (id)splitViewDetailWithBarButtonItem
{
    id detailViewController = [self.splitViewController.viewControllers lastObject];
    if (![detailViewController respondsToSelector:@selector(setSplitViewBarButtonItem:)] ||
        ![detailViewController respondsToSelector:@selector(splitViewBarButtonItem)]) detailViewController = nil;
    return detailViewController;
}
- (void)transferSplitViewBarButtonItemToViewController:(id)destinationViewController
{
    UIBarButtonItem *splitViewBarButtonItem = [[self splitViewDetailWithBarButtonItem] performSelector:@selector(splitViewBarButtonItem)];
    [[self splitViewDetailWithBarButtonItem] performSelector:@selector(setSplitViewBarButtonItem:) withObject:nil];
    if (splitViewBarButtonItem)
        [destinationViewController performSelector:@selector(setSplitViewBarButtonItem:) withObject:splitViewBarButtonItem];
}

@end
