//
//  PhotosByTagCDTVC.h
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "PhotosCDTVC.h"
#import "Tag.h"

@interface PhotosByTagCDTVC : PhotosCDTVC

@property (nonatomic, strong) Tag *tag;

@end
