//
//  CoreDataHelper.h
//  CDSPoT
//
//  Created by Juan C. Catalan on 12/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonManagedDocument : NSObject

// property used to store the shard UIManagedDocument 
@property (readonly, strong, nonatomic) UIManagedDocument *sharedDocument;

// return the shared instance of this class (Singleton pattern)
+ (SingletonManagedDocument *)sharedManagedDocument;

@end
