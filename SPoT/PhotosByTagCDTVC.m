//
//  PhotosByTagCDTVC.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "PhotosByTagCDTVC.h"
#import "Photo.h"
#import "Tag+Create.h"

@implementation PhotosByTagCDTVC

- (void)setTag:(Tag *)tag
{
    _tag = tag;
    // capitalize the tag and remove any unwanted symbol characters
    self.title = [[tag.name capitalizedString] stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]];
    [self setupFetchedResultsController];
}

- (void)setupFetchedResultsController
{
    if (self.tag.managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)],[NSSortDescriptor sortDescriptorWithKey:@"subtitle" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = [NSPredicate predicateWithFormat:@"%@ IN tags AND displayed=YES",self.tag];
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.tag.managedObjectContext sectionNameKeyPath:@"section" cacheName:nil];
    }
}

@end
