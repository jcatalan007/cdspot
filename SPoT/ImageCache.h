//
//  PhotoCache.h
//  SPoT
//
//  Created by Juan C. Catalan on 01/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCache : NSObject

+ (NSData *)imageFromURL:(NSURL *)url;
+ (void)storeImage:(NSData *)imageData forURL:(NSURL *)imageURL;

@end
