//
//  RecentPhotosCDTVC.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 12/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "RecentPhotosCDTVC.h"
#import "SingletonManagedDocument.h"

@interface RecentPhotosCDTVC()

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation RecentPhotosCDTVC

#define MAX_RECENT_PHOTOS_TO_SHOW 5

- (void)reloadRecentList
{
    if (self.managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"dateStamp" ascending:NO]];
        request.predicate = [NSPredicate predicateWithFormat:@"dateStamp != NULL AND displayed = YES"];
        request.fetchLimit = MAX_RECENT_PHOTOS_TO_SHOW;
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    [self reloadRecentList];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!self.managedObjectContext)
        self.managedObjectContext = [SingletonManagedDocument sharedManagedDocument].sharedDocument.managedObjectContext;
}

@end
