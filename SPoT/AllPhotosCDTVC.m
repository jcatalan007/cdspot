//
//  AllPhotosCDTVC.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 14/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "AllPhotosCDTVC.h"
#import "Photo.h"
#import "Tag+Create.h"

@implementation AllPhotosCDTVC

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    self.title = @"Photos by tag";
    if (managedObjectContext) {
        NSFetchRequest *requestTags = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
        requestTags.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
        requestTags.predicate = [NSPredicate predicateWithFormat:@"name!=%@ AND displayed=YES",[Tag catchallTagName]];
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:requestTags managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"capitalizedName" cacheName:nil];
        [self.fetchedResultsController performFetch:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (Photo *)photoForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSSet *photosInSection = ((Tag *)[[self.fetchedResultsController fetchedObjects] objectAtIndex:indexPath.section]).photos;
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"displayed=YES"];
    NSSet *visiblePhotosInSection = [photosInSection filteredSetUsingPredicate:filterPredicate];
    NSArray *sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)],[NSSortDescriptor sortDescriptorWithKey:@"subtitle" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
    NSArray *sortedVisiblePhotosInSection = [visiblePhotosInSection sortedArrayUsingDescriptors:sortDescriptors];
    return sortedVisiblePhotosInSection[indexPath.row];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[[self.fetchedResultsController fetchedObjects] objectAtIndex:section] displayedPhotos] count];
}

@end
