//
//  UIApplication+NetworkActivity.h
//  SPoT
//
//  Created by Juan C. Catalan on 04/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (NetworkActivity)

- (void)showNetworkActivityIndicator;
- (void)hideNetworkActivityIndicator;

@end
