//
//  Photo+Flickr.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Photo+Flickr.h"
#import "FlickrFetcher.h"
#import "Tag+Create.h"

@implementation Photo (Flickr)

+ (Photo *)photoWithFlickrInfo:(NSDictionary *)photoDictionary
        inManagedObjectContext:(NSManagedObjectContext *)context
{
    
    // Query the database to check if that particular photo already exists
    // Looking for a photo
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    // Sort results by photo.title in ascending order
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]];
    // Filter photos by property unique that uniquely identifies the Flickr photo
    // This unique value is provided by Flickr
    request.predicate = [NSPredicate predicateWithFormat:@"unique = %@",[photoDictionary[FLICKR_PHOTO_ID] description]];
    
    NSError *error;
    // Send the request to the database engine
    NSArray *matches = [context executeFetchRequest:request error:&error];
    
    // Create the photo if it doesn't exist or return it if it is found in the database
    Photo *photo;
    if (!matches || ([matches count]>1)) {
        // handle error
    } else if (![matches count]) {
        // the photo is not in the database so we create it
        photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];        
        photo.unique = [photoDictionary[FLICKR_PHOTO_ID] description];
        photo.title = [photoDictionary[FLICKR_PHOTO_TITLE] description];
        // the section property is created for the purpose of having sections in the tableView that are the first capitalized letter of the photo.title
        // it's value is a NSString of 1 character, the first character in photo.title capitalized
        photo.subtitle = [[photoDictionary valueForKeyPath:FLICKR_PHOTO_DESCRIPTION] description];
        photo.displayed = @(YES);
        // Set the flickr photo format to use depending on type of device
        FlickrPhotoFormat flickrFormat = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? FlickrPhotoFormatOriginal : FlickrPhotoFormatLarge;
        photo.imageURL = [[FlickrFetcher urlForPhoto:photoDictionary format:flickrFormat] absoluteString];
        // Set the thumbnail URL for future use
        photo.thumbnailURL = [[FlickrFetcher urlForPhoto:photoDictionary format:FlickrPhotoFormatSquare] absoluteString];
        // Get the tag names for this photo
        NSArray *tagNames = [[self class] tagNamesForFlickerPhoto:photoDictionary];
        // Create a NSSet with all tags that a photo has
        NSMutableSet *tags = [[NSMutableSet alloc] init];
        for (NSString *name in tagNames) {
            // Create the tag in the database (or get it if already exists)
            Tag *tag = [Tag tagWithName:name inManagedObjectContext:context];
            // Add the tag to the set of tags for this photo
            [tags addObject:tag];
        }
        // Set the relationship of the photo with its tags
        photo.tags = tags;
    } else {
        // the photo is already in the database
        photo = matches[0];
    }    
    return photo;
}

#define CATCH_ALL_TAG_NAME  // Catch-all tag to be added to every photo

// returns an NSArray of NSStrings representing the tagNames for the given Flicker photo
+ (NSArray *)tagNamesForFlickerPhoto:(NSDictionary *)photoDictionary
{
    // get the tags for the photo
    NSArray *tags = [photoDictionary[FLICKR_TAGS] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // add the catch-all tag to the list of tags
    return [tags arrayByAddingObject:[Tag catchallTagName]];
}

- (NSString *)section
{
    // the property 'section' is created for the purpose of having sections in the tableView
    // the section is a NSString of 1 character that contains the first capitalized letter of the photo.title capitalized
    return [[self.title substringToIndex:1] capitalizedString];
}

@end
