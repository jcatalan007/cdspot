//
//  TagCDTVC.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//
//  Can do "setTag:" segue and will call setTag: method in destination VC

#import "TagCDTVC.h"
#import "Tag.h"

@implementation TagCDTVC

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        if (indexPath) {
            if ([segue.identifier isEqualToString:@"setTag:"]) {
                Tag *tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
                if ([segue.destinationViewController respondsToSelector:@selector(setTag:)]) {
                    [segue.destinationViewController performSelector:@selector(setTag:) withObject:tag];
                }
            } else if ([segue.identifier isEqualToString:@"setAll:"]) {
                if ([segue.destinationViewController respondsToSelector:@selector(setManagedObjectContext:)]) {
                    [segue.destinationViewController performSelector:@selector(setManagedObjectContext:) withObject:self.managedObjectContext];
                }
            }
        }
    }
}

- (void)setManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    _managedObjectContext = managedObjectContext;
    if (managedObjectContext) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)]];
        request.predicate = [NSPredicate predicateWithFormat:@"displayed=YES"];
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:managedObjectContext sectionNameKeyPath:@"section" cacheName:nil];
    } else {
        self.fetchedResultsController = nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.section==0) 
        cell = [tableView dequeueReusableCellWithIdentifier:@"All"];        
    else
        cell = [tableView dequeueReusableCellWithIdentifier:@"Tag"];
    
    Tag *tag = [self.fetchedResultsController objectAtIndexPath:indexPath];
    // the text to display as the title is capitalized and trimmed from any symbol
    cell.textLabel.text = [[tag.name stringByTrimmingCharactersInSet:[NSCharacterSet symbolCharacterSet]] capitalizedString];
    // filter the photos by displayed property in order to obtain the count of photos that are displayed
    NSPredicate *filterDisplayed = [NSPredicate predicateWithFormat:@"displayed=YES"];
    NSSet *displayedPhotos = [tag.photos filteredSetUsingPredicate:filterDisplayed];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%d photo%@", [displayedPhotos count], ([displayedPhotos count]==1) ? @"" : @"s"];
    return cell;
}

@end
