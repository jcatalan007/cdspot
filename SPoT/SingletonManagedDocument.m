//
//  CoreDataHelper.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 12/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//
//  Thanks to Michael Grysikiewicz for providing this code

#import "SingletonManagedDocument.h"

@interface SingletonManagedDocument()

@property (readwrite, strong, nonatomic) UIManagedDocument *sharedDocument;

@end

@implementation SingletonManagedDocument

+ (SingletonManagedDocument *)sharedManagedDocument
{
    static dispatch_once_t once;
    static id _sharedInstance;
    dispatch_once(&once, ^{
        _sharedInstance = [[SingletonManagedDocument alloc] init];
    });
    return _sharedInstance;
}

- (UIManagedDocument *)sharedDocument
{
    // lazy instantiation
    if (!_sharedDocument) {
        NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        url = [url URLByAppendingPathComponent:@"StanfordDocument"];
        _sharedDocument = [[UIManagedDocument alloc] initWithFileURL:url];
    }    
    return _sharedDocument;
}

@end
