//
//  UIApplication+NetworkActivity.m
//  SPoT
//
//  Created by Juan C. Catalan on 04/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//
//  Solution from: http://stackoverflow.com/questions/3032192/networkactivityindicatorvisible
//  Pointed out by: Michael Grysikiewicz


#import "UIApplication+NetworkActivity.h"

static NSInteger activityCount = 0;

@implementation UIApplication (NetworkActivity)

- (void)showNetworkActivityIndicator {
    if (![[UIApplication sharedApplication] isStatusBarHidden])
        @synchronized ([UIApplication sharedApplication]) {
            if (activityCount++ == 0)
                self.networkActivityIndicatorVisible = YES;
        }
}

- (void)hideNetworkActivityIndicator {
    if (![[UIApplication sharedApplication] isStatusBarHidden]) 
        @synchronized ([UIApplication sharedApplication]) {
            if (--activityCount <= 0) {
                self.networkActivityIndicatorVisible = NO;
                activityCount = 0;
            }
        }
}

@end
