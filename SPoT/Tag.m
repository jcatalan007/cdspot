//
//  Tag.m
//  CDSPoT
//
//  Created by Juan C. Catalan on 15/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "Tag.h"
#import "Photo.h"


@implementation Tag

@dynamic name;
@dynamic displayed;
@dynamic photos;

@end
