//
//  PhotoCell.h
//  CDSPoT
//
//  Created by Juan C. Catalan on 23/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UITableViewCell

@property (strong, nonatomic) NSString *photoId;

@end
