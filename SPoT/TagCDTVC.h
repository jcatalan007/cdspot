//
//  TagCDTVC.h
//  CDSPoT
//
//  Created by Juan C. Catalan on 11/03/13.
//  Copyright (c) 2013 CS193p. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface TagCDTVC : CoreDataTableViewController

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;

@end
